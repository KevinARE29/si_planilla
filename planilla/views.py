from django.db.transaction import commit
from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Q #Para condiciones OR en Queryset
from .models import  *
from django.urls import reverse_lazy, reverse
from django.contrib import messages
from django.http import HttpResponseRedirect
from material import *
from django.http import JsonResponse
from django.http import HttpResponse
import json
from django import forms
from .forms import *
from decimal import Decimal
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import redirect
from django.db import connection
from decimal import Decimal
import json
import datetime
from django.db.models import Sum
from django.db.models.functions import Coalesce

# Necesario para la generación de PDF
from django.views import View
from wkhtmltopdf.views import PDFTemplateResponse

# Create your views here.
class Inicio(LoginRequiredMixin, TemplateView):
    template_name = "base.html"

# Inician vistas para Estado Civil
class AgregarEstadoCivil(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = EstadoCivil
    fields = ['nombre']
    template_name = 'planilla/agregar_estado_civil.html'
    success_message = "Estado Civil Agregado con Éxito"
    permission_required = 'planilla.add_estadocivil'
    success_url = reverse_lazy('agregar_estadocivil')

    layout = Layout(Fieldset('Agregar Estado Civil: '), Row('nombre'))

@login_required
def estadosciviles(request):
    mensaje = ""
    estados = EstadoCivil.objects.all()

    return render(request, 'planilla/estados.html', {'estados': estados, 'mensaje': mensaje, })

class ActualizarEstadoCivil(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = EstadoCivil
    fields = ['nombre']
    template_name = 'planilla/agregar_estado_civil.html'
    success_message = "Estado Civil Modificado con Éxito "
    permission_required = 'planilla.change_estadocivil'

    layout = Layout(Fieldset('Actualizar Estado Civil: '), Row('nombre'))

    def get_context_data(self, **kwargs):
        context = super(ActualizarEstadoCivil, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("estadosciviles")

@login_required
def desactivar_estadocivil(request, pk):
    estado = EstadoCivil.objects.get(pk=pk)
    if estado.activo:
        estado.activo = False
    else:
        estado.activo = True
    estado.save()
    messages.add_message(request, messages.INFO, 'Cambio de estado exitoso')

    return HttpResponseRedirect(reverse("estadosciviles"))
# Finalizan vistas para Estado Civil

# Inician vistas para Profesión
class AgregarProfesion(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = Profesion
    fields = ['nombre']
    template_name = 'planilla/agregar_profesion.html'
    success_message = "Profesión Agregada con Éxito"
    permission_required = 'planilla.add_profesion'
    success_url = reverse_lazy('agregar_profesion')

    layout = Layout(Fieldset('Agregar Profesion: '), Row('nombre'))

@login_required
def profesiones(request):
    mensaje = ""
    if request.user.has_perm('planilla.change_profesion') or request.user.has_perm('planilla.delete_profesion'):
        profesiones = Profesion.objects.all()
        return render(request, 'planilla/profesiones.html', {'profesiones': profesiones, 'mensaje': mensaje, })
    else:
        mensaje = "No posee permisos para acceder a la página solicitada. Para continuar inicie sesión con un usuario privilegiado"
        return render(request, 'autenticacion/login.html', {'mensaje': mensaje, })

class ActualizarProfesion(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = Profesion
    fields = ['nombre']
    template_name = 'planilla/agregar_profesion.html'
    success_message = "Profesión Modificada con Éxito "
    permission_required = 'planilla.change_profesion'

    layout = Layout(Fieldset('Actualizar Profesion: '), Row('nombre'))

    def get_context_data(self, **kwargs):
        context = super(ActualizarProfesion, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("profesiones")

@login_required
def desactivar_profesion(request, pk):
    profesion = Profesion.objects.get(pk=pk)
    if profesion.activo:
        profesion.activo = False
    else:
        profesion.activo = True
    profesion.save()
    messages.add_message(request, messages.INFO, 'Cambio de estado exitoso')

    return HttpResponseRedirect(reverse("profesiones"))
# Finalizan vistas para Profesión

# Inician vistas para Comisiones

def AgregarComisiones(request):
    mensaje=""
    ultima_comision = None
    try:
        ultima_comision = Comision.objects.last()
        desde = ultima_comision.lim_superior + Decimal('0.01')

    except:
        desde = 100.00

    # Cuando es Get
    if request.method=='GET':
        comision_form=AgregarComisionForm(initial={'lim_inferior':desde})

    #Cuando es POST
    if request.method=='POST':
        comision_form=AgregarComisionForm(request.POST or None)

        if comision_form.is_valid():
            hasta = comision_form.cleaned_data['lim_superior']
            porcentaje = comision_form.cleaned_data['porcentaje']

            if desde<hasta:
                comision =Comision.objects.create(
                    lim_inferior=desde,
                    lim_superior=hasta,
                    porcentaje = porcentaje
                )
                mensaje = "Comision Creada con Exito"

            else:
                mensaje = "El limite inferior debe ser menor al limite superior"
            #escala_form = AgregarEscalaForm(initial={'desde': hasta + Decimal('0.01'), 'exceso': hasta}, prefix='escala')

        try:
            ultima_comision = Comision.objects.last()
            desde = ultima_comision.lim_superior + Decimal('0.01')
        except:
            desde = 100.00

        comision_form = AgregarComisionForm(initial={'lim_inferior': desde})

    extra_context = {
        'form': comision_form,
        'mensaje': mensaje
    }

    return render(request, 'planilla/agregar_comision.html' ,extra_context)

def ActualizarComisiones(request, pk):
    mensaje = ""
    escala = Comision.objects.get(pk=pk)

    form = AgregarComisionForm(data=request.POST or None, instance=escala)
    siguientes = Comision.objects.all().order_by('lim_inferior')
    nextescala = escala.lim_superior + Decimal('0.01')
    siguescala = None
    hasta = None
    try:
        siguescala = Comision.objects.get(lim_inferior=nextescala)
    except:
        pass
    choques = []
    if form.is_valid():
        hasta = form.cleaned_data['lim_superior']
        for siguiente in siguientes:
            if siguiente.pk > int(pk):
                if siguiente.lim_superior < hasta:
                    choques.append(siguiente)
        if not choques:
            form.save()
            if siguescala:
                siguescala.lim_inferior = hasta + Decimal('0.01')
                siguescala.save()
            mensaje = "Comision Actualizada con Éxito"
            return HttpResponseRedirect('/comisiones')

    return render(request, 'planilla/actualizar_comisiones.html', {'form': form, 'mensaje': mensaje, 'choques': choques, 'pk':pk, 'hasta':hasta})

def eliminar_comisiones(request, pk, hasta):
    mensaje = ""
    a = request.POST
    escala = Comision.objects.get(pk=pk)
    escala.lim_superior = hasta
    escala.save()
    siguientes = Comision.objects.all().order_by('lim_inferior')
    choques = []
    nextval = 0
    siguescala = None
    hasta = Decimal(hasta)
    for siguiente in siguientes:
        if siguiente.pk > int(pk):
            if siguiente.lim_superior < hasta:
                nextval = siguiente.lim_superior
                siguiente.delete()
    nextescala = nextval + Decimal('0.01')
    try:
        siguescala = Comision.objects.get(lim_inferior=nextescala)
    except:
        pass
    if siguescala:
        siguescala.lim_inferior = hasta + Decimal('0.01')
        siguescala.save()
    mensaje = "Comision Actualizada con Éxito"
    return HttpResponseRedirect('/comisiones')

class AgregarComision(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = Comision
    fields = ['lim_inferior', 'lim_superior', 'porcentaje']
    template_name = 'planilla/agregar_comision.html'
    success_message = "Tasa de Comisión Agregada con Éxito"
    permission_required = 'planilla.add_comision'
    success_url = reverse_lazy('agregar_comision')

    layout = Layout(Fieldset('Agregar Tasa de Comision: '), Row('lim_inferior', 'lim_superior'), Row('porcentaje'))

@login_required
def comisiones(request):
    mensaje = ""
    if request.user.has_perm('planilla.change_comision') or request.user.has_perm('planilla.delete_comision'):
        comisiones = Comision.objects.all()
        return render(request, 'planilla/comisiones.html', {'comisiones': comisiones, 'mensaje': mensaje, })
    else:
        mensaje = "No posee permisos para acceder a la página solicitada. Para continuar inicie sesión con un usuario privilegiado"
        return render(request, 'autenticacion/login.html', {'mensaje': mensaje, })

class ActualizarComision(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = Comision
    fields = ['lim_inferior', 'lim_superior', 'porcentaje']
    template_name = 'planilla/agregar_comision.html'
    success_message = "Tasa de Comisión Modificada con Éxito "
    permission_required = 'planilla.change_comision'

    layout = Layout(Fieldset('Actualizar Tasa de Comisión: '), Row('lim_inferior', 'lim_superior'), Row('porcentaje'))

    def get_context_data(self, **kwargs):
        context = super(ActualizarComision, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("comisiones")

@login_required
def eliminar_comision(request, pk):
    try:
        comision = Comision.objects.get(pk=pk)
        comision.delete()
        messages.add_message(request, messages.INFO, 'Tasa de Comisión Eliminada con éxito')
    except:
        messages.add_message(request, messages.INFO, 'Error al eliminar la Tasa de Comisión')

    return HttpResponseRedirect('/comisiones')
# Finalizan vistas para Comisiones


# Inician vistas para Tipos de Ingreso
class AgregarTipoIngreso(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = TipoIngreso
    fields = ['nombre', 'gravado']
    template_name = 'planilla/agregar_tipo_ingreso.html'
    success_message = "Tipo de Ingreso Agregado con Éxito"
    permission_required = 'planilla.add_tipoingreso'
    success_url = reverse_lazy('agregar_tipoingreso')

    layout = Layout(Fieldset('Agregar Tipo de Ingreso: '), Row('nombre', 'gravado'))

@login_required
def ingresos(request):
    mensaje = ""
    if request.user.has_perm('planilla.change_tipoingreso') or request.user.has_perm('planilla.delete_tipoingreso'):
        ingresos = TipoIngreso.objects.all()
        return render(request, 'planilla/ingresos.html', {'ingresos': ingresos, 'mensaje': mensaje, })
    else:
        mensaje = "No posee permisos para acceder a la página solicitada. Para continuar inicie sesión con un usuario privilegiado"
        return render(request, 'autenticacion/login.html', {'mensaje': mensaje, })

class ActualizarTipoIngreso(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = TipoIngreso
    fields = ['nombre', 'gravado']
    template_name = 'planilla/agregar_tipo_ingreso.html'
    success_message = "Tipo de Ingreso Modificado con Éxito "
    permission_required = 'planilla.change_tipoingreso'

    layout = Layout(Fieldset('Actualizar Tipo de Ingreso: '), Row('nombre', 'gravado'))

    def get_context_data(self, **kwargs):
        context = super(ActualizarTipoIngreso, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("ingresos")

@login_required
def desactivar_tipoingreso(request, pk):
    try:
        ingreso = TipoIngreso.objects.get(pk=pk)
        ingreso.activo = not ingreso.activo
        ingreso.save()
        messages.add_message(request, messages.INFO, 'Cambio de Estado Realizado con éxito')
    except:
        messages.add_message(request, messages.INFO, 'Error al Cambiar Estado')

    return HttpResponseRedirect('/ingresos')
# Finalizan vistas para Comisiones

#Inician vistas para Estructura Territorial
@login_required
def Estructura_territorial(self):
    return render(self, 'planilla/estructura_territorial.html')

def obtener_division(request):
    root = request.POST
    pk = int(root.get("super"))
    if pk == 0:
        divisiones = list(EstructuraTerritorial.objects.filter(superdivision__isnull=True).values())
    else:
        divisiones = list(EstructuraTerritorial.objects.filter(superdivision=pk).values())
    return JsonResponse({ "divisiones": divisiones})

def guardar_division(request):
    if request.method == "POST" and request.is_ajax():
        root = request.POST;
        p = EstructuraTerritorial(nombre = root.get("nombre"), superdivision_id = root.get("super"));
        p.save();
        return JsonResponse({"id": p.id,"nombre": p.nombre, "super": p.superdivision_id})
    else:
        return JsonResponse({"message": "Consulta sin autorización"})

def eliminar_division(request):
    if request.method == "POST" and request.is_ajax():
        super = int(request.POST.get("division"))
        id = int(request.POST.get("id"))
        if super == 3:
            EstructuraTerritorial.objects.filter(id=id).delete()
        else:
            EstructuraTerritorial.objects.filter(superdivision_id=id).delete()
            EstructuraTerritorial.objects.filter(id=id).delete()
        return JsonResponse({"message": "Borrado con éxito"})
    else:
        return JsonResponse({"message": "Consulta sin autorización"})
#Terminan vistas de Estructura Territorial

# Inician vistas para Genero
class AgregarGenero(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = Genero
    fields = ['nombre']
    template_name = 'planilla/agregar_genero.html'
    success_message = "Genero Agregada con Éxito"
    permission_required = 'planilla.add_genero'
    success_url = reverse_lazy('agregar_genero')

    layout = Layout(Fieldset('Agregar Genero: '), Row('nombre'))

@login_required
def generos(request):
    mensaje = ""
    if request.user.has_perm('planilla.change_genero') or request.user.has_perm('planilla.delete_genero'):
        generos = Genero.objects.all()
        return render(request, 'planilla/generos.html', {'generos': generos, 'mensaje': mensaje, })
    else:
        mensaje = "No posee permisos para acceder a la página solicitada. Para continuar inicie sesión con un usuario privilegiado"
        return render(request, 'autenticacion/login.html', {'mensaje': mensaje, })

class ActualizarGenero(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = Genero
    fields = ['nombre']
    template_name = 'planilla/agregar_genero.html'
    success_message = "Genero Modificada con Éxito "
    permission_required = 'planilla.change_genero'

    layout = Layout(Fieldset('Actualizar Genero: '), Row('nombre'))

    def get_context_data(self, **kwargs):
        context = super(ActualizarGenero, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("generos")

@login_required
def desactivar_genero(request, pk):
    genero = Genero.objects.get(pk=pk)
    if genero.activo:
        genero.activo = False
    else:
        genero.activo = True
    genero.save()
    messages.add_message(request, messages.INFO, 'Cambio de estado exitoso')

    return HttpResponseRedirect(reverse("generos"))
# Finalizan vistas para Genero

# Inician vistas para Empleado
class AgregarEmpleado(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = Empleado
    fields = ['primer_nombre', 'segundo_nombre', 'apellido_paterno', 'apellido_materno', 'apellido_casada', 'dui', 'nit', 'nup', 'isss', 'pasaporte',
              'fecha_nacimiento', 'fecha_ingreso', 'telefono', 'email_personal', 'email_institucional', 'direccion', 'genero', 'estado_civil',
              'jefe', 'puesto', 'profesion', 'salario_fijo', 'domicilio', 'salario', 'unidad_organizacional']
    template_name = 'planilla/agregar_empleado.html'
    success_message = "Empleado agregado con éxito"
    permission_required = 'planilla.add_empleado'
    success_url = reverse_lazy('empleados')

    layout = Layout(Fieldset('Agregar Empleado: '), Row('primer_nombre', 'segundo_nombre'), Row('apellido_paterno', 'apellido_materno','apellido_casada'), Row('dui', 'nit'), Row('nup', 'isss', 'pasaporte'),
                    Row('fecha_nacimiento', 'fecha_ingreso', 'telefono'), Row('email_personal', 'email_institucional'), Row('direccion', 'domicilio'), Row('genero','estado_civil'),
                    Row('profesion', 'salario_fijo'), Row('unidad_organizacional', 'puesto'),  Row('salario', 'jefe'))

    def get_context_data(self, **kwargs):
        context = super(AgregarEmpleado, self).get_context_data(**kwargs)
        context['fechas'] = ["fecha_nacimiento", "fecha_ingreso"]
        return context

    def form_valid(self, form):
        try:
            return super(AgregarEmpleado, self).form_valid(form)
        except Exception as e:
            field = e.args[0].split('\n')[1].replace('HINT:  ', '')
            mensaje = e.args[0].split('\n')[0]
            form.add_error(field, mensaje)
            return super(AgregarEmpleado, self).form_invalid(form)

@login_required
def empleados(request):
    mensaje = ""
    if request.user.has_perm('planilla.change_empleado') or request.user.has_perm('planilla.delete_profesion'):
        empleados = Empleado.objects.all()
        return render(request, 'planilla/empleados.html', {'empleados': empleados, 'mensaje': mensaje, })
    else:
        mensaje = "No posee permisos para acceder a la página solicitada. Para continuar inicie sesión con un usuario privilegiado"
        return render(request, 'autenticacion/login.html', {'mensaje': mensaje, })

def subordinados(request, id):
    mensaje = ""
    empleados = Empleado.objects.filter(jefe=id)
    return render(request, 'planilla/empleados.html', {'empleados': empleados, 'mensaje': mensaje, })

class ActualizarEmpleado(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = Empleado
    fields = ['primer_nombre', 'segundo_nombre', 'apellido_paterno', 'apellido_materno', 'apellido_casada', 'dui',
              'nit', 'nup', 'isss', 'pasaporte',
              'fecha_nacimiento', 'fecha_ingreso', 'telefono', 'email_personal', 'email_institucional', 'direccion',
              'genero', 'estado_civil',
              'jefe', 'puesto', 'profesion','salario_fijo', 'domicilio', 'salario', 'unidad_organizacional']
    template_name = 'planilla/agregar_empleado.html'
    success_message = "Empleado actualizado con éxito"
    permission_required = 'planilla.change_empleado'

    layout = Layout(Fieldset('Agregar Empleado: '), Row('primer_nombre', 'segundo_nombre'),
                    Row('apellido_paterno', 'apellido_materno', 'apellido_casada'), Row('dui', 'nit'),
                    Row('nup', 'isss', 'pasaporte'),
                    Row('fecha_nacimiento', 'fecha_ingreso', 'telefono'), Row('email_personal', 'email_institucional'),
                    Row('direccion', 'domicilio'), Row('genero', 'estado_civil'),
                    Row('profesion', 'salario_fijo'), Row('unidad_organizacional', 'puesto'),  Row('salario', 'jefe'))

    def get_context_data(self, **kwargs):
        context = super(ActualizarEmpleado, self).get_context_data(**kwargs)
        context['actualizar'] = True
        context['fechas'] = ["fecha_nacimiento", "fecha_ingreso"]
        return context

    def get_success_url(self):
        return reverse("empleados")

def desactivar_empleado(request, pk):
    empleado = Empleado.objects.get(pk=pk)
    empleado.activo = not empleado.activo
    empleado.save()
    messages.add_message(request, messages.INFO, 'Cambio de estado exitoso')

    return HttpResponseRedirect(reverse("empleados"))
# Finalizan vistas para Empleado

# Inician vistas para Descuento Personal
class AgregarDescuento(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = DescuentoPersonal
    fields = ['nombre']
    template_name = 'planilla/agregar_descuento.html'
    success_message = "Descuento Personal Agregado con Éxito"
    permission_required = 'planilla.add_descuentopersonal'
    success_url = reverse_lazy('agregar_descuento')

    layout = Layout(Fieldset('Agregar Descuento Personal: '), Row('nombre'))

@login_required
def descuentos(request):
    mensaje = ""
    if request.user.has_perm('planilla.change_descuentopersonal') or request.user.has_perm('planilla.delete_descuentopersonal'):
        descuentos = DescuentoPersonal.objects.all()
        return render(request, 'planilla/descuentos.html', {'descuentos': descuentos, 'mensaje': mensaje, })
    else:
        mensaje = "No posee permisos para acceder a la página solicitada. Para continuar inicie sesión con un usuario privilegiado"
        return render(request, 'autenticacion/login.html', {'mensaje': mensaje, })

class ActualizarDescuento(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = DescuentoPersonal
    fields = ['nombre']
    template_name = 'planilla/agregar_descuento.html'
    success_message = "Descuento Personal Modificado con Éxito "
    permission_required = 'planilla.change_descuentopersonal'

    layout = Layout(Fieldset('Actualizar Descuento Personal: '), Row('nombre'))

    def get_context_data(self, **kwargs):
        context = super(ActualizarDescuento, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("descuentos")

def desactivar_descuento(request, pk):
    descuento = DescuentoPersonal.objects.get(pk=pk)
    if descuento.activo:
        descuento.activo = False
    else:
        descuento.activo = True
    descuento.save()
    messages.add_message(request, messages.INFO, 'Cambio de estado exitoso')

    return HttpResponseRedirect(reverse("descuentos"))
# Finalizan vistas para Descuento Personal

# Inician vistas para Cotización
class AgregarCotizacion(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = Cotizacion
    form_class = CotizacionForm
    template_name = 'planilla/agregar_cotizacion.html'
    success_message = "Cotización Agregada con Éxito"
    permission_required = 'planilla.add_cotizacion'
    success_url = reverse_lazy('agregar_cotizacion')

    layout = Layout(Fieldset('Agregar Cotizacion: '), Row('nombre', 'porcentaje'))

@login_required
def cotizaciones(request):
    mensaje = ""
    if request.user.has_perm('planilla.change_profesion') or request.user.has_perm('planilla.delete_profesion'):
        cotizaciones = Cotizacion.objects.all()
        return render(request, 'planilla/cotizaciones.html', {'cotizaciones': cotizaciones, 'mensaje': mensaje, })
    else:
        mensaje = "No posee permisos para acceder a la página solicitada. Para continuar inicie sesión con un usuario privilegiado"

class ActualizarCotizacion(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = Cotizacion
    fields = ['nombre', 'porcentaje']
    template_name = 'planilla/agregar_cotizacion.html'
    success_message = "Cotización Modificada con Éxito "
    permission_required = 'planilla.change_cotizacion'

    layout = Layout(Fieldset('Actualizar Cotizacion: '), Row('nombre', 'porcentaje'))

    def get_context_data(self, **kwargs):
        context = super(ActualizarCotizacion, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("cotizaciones")


def desactivar_cotizacion(request, pk):
    cotizacion = Cotizacion.objects.get(pk=pk)
    if cotizacion.activo:
        cotizacion.activo = False
    else:
        cotizacion.activo = True
    cotizacion.save()
    messages.add_message(request, messages.INFO, 'Cambio de estado exitoso')

    return HttpResponseRedirect(reverse("cotizaciones"))
# Finalizan vistas para Cotización

# Inician Vistas para Renta

def AgregarEscala(request):
    mensaje=""
    ultima_escala = None
    ultima_escala_15 = None
    try:
        ultima_escala = Renta.objects.filter(tipo='2').last()
        desde = ultima_escala.hasta + Decimal('0.01')
        exceso = ultima_escala.hasta
    except:
        desde = 0.01
        exceso = 0.00

    try:
        ultima_escala_15 = Renta.objects.filter(tipo='1').last()
        desde_15 = ultima_escala_15.hasta + Decimal('0.01')
        exceso_15 = ultima_escala_15.hasta
    except:
        desde_15 = 0.01
        exceso_15 = 0.00

    # Cuando es Get
    if request.method=='GET':
        if ultima_escala_15:
            escala_form=AgregarEscalaForm(initial={'desde':desde_15, 'exceso':ultima_escala_15.hasta},prefix='escala')
        else:
            escala_form = AgregarEscalaForm(initial={'desde': desde_15}, prefix='escala')

    #Cuando es POST
    if request.method=='POST':
        escala_form=AgregarEscalaForm(request.POST or None, prefix='escala')

        if escala_form.is_valid():
            if request.POST.get('escala-tipo') == '1':
                desdeG = desde_15
                excesoG = exceso_15
            else:
                desdeG = desde
                excesoG = exceso
            hasta = escala_form.cleaned_data['hasta']
            porcentaje = escala_form.cleaned_data['porcentaje']
            cuota_fija = escala_form.cleaned_data['cuota_fija']
            tipo = escala_form.cleaned_data['tipo']

            if desdeG<hasta:
                escala =Renta.objects.create(
                    desde = desdeG,
                    hasta = hasta,
                    porcentaje = porcentaje,
                    exceso = excesoG,
                    cuota_fija=cuota_fija,
                    tipo=tipo
                )
                mensaje = "Escala de Renta Creada con Exito"

            else:
                mensaje = "La cantidad 'desde' debe ser menor a la cantidad 'hasta'"
            #escala_form = AgregarEscalaForm(initial={'desde': hasta + Decimal('0.01'), 'exceso': hasta}, prefix='escala')

        try:
            ultima_escala = Renta.objects.filter(tipo='2').last()
            desde = ultima_escala.hasta + Decimal('0.01')
            exceso = ultima_escala.hasta
        except:
            desde = 0.01
            exceso = 0.00

        try:
            ultima_escala_15 = Renta.objects.filter(tipo='1').last()
            desde_15 = ultima_escala_15.hasta + Decimal('0.01')
            exceso_15 = ultima_escala_15.hasta
        except:
            desde_15 = 0.01
            exceso_15 = 0.00
        if request.POST.get('escala-tipo') == '1':
            escala_form = AgregarEscalaForm(initial={'desde': desde_15, 'exceso': exceso_15}, prefix='escala')
        else:
            escala_form = AgregarEscalaForm(initial={'desde': desde, 'exceso': exceso}, prefix='escala')

    extra_context = {
        'form': escala_form,
        'mensaje': mensaje,
        'desde': [str(desde_15), str(desde)],
        'exceso': [str(exceso_15), str(exceso)]
    }

    return render(request, 'planilla/agregar_escala_renta.html' ,extra_context)


@login_required
def escalas(request):
    mensaje = ""
    escalas_15 = Renta.objects.filter(tipo='1').order_by('desde')
    escalas_30 = Renta.objects.filter(tipo='2').order_by('desde')

    return render(request, 'planilla/escalas.html', {'escalas_15': escalas_15, 'escalas_30': escalas_30, 'mensaje': mensaje, })

def ActualizarEscala(request, pk):
    mensaje = ""
    escala = Renta.objects.get(pk=pk)
    tipo = escala.tipo
    form = AgregarEscalaForm(data=request.POST or None, instance=escala)
    siguientes = Renta.objects.filter(tipo=tipo).order_by('desde')
    nextescala = escala.hasta + Decimal('0.01')
    siguescala = None
    hasta = None
    try:
        siguescala = Renta.objects.filter(tipo=tipo).get(desde=nextescala)
    except:
        pass
    choques = []
    if form.is_valid():
        hasta = form.cleaned_data['hasta']
        for siguiente in siguientes:
            if siguiente.pk > int(pk):
                if siguiente.hasta < hasta:
                    choques.append(siguiente)
        if not choques:
            form.save()
            if siguescala:
                siguescala.desde = hasta + Decimal('0.01')
                siguescala.exceso = hasta
                siguescala.save()
            mensaje = "Tramo Actualizado con Éxito"
            return HttpResponseRedirect('/escalas')

    return render(request, 'planilla/actualizar_escala_renta.html', {'form': form, 'mensaje': mensaje, 'choques': choques, 'pk':pk, 'hasta':hasta})


def eliminar_tramos(request, pk, hasta):
    mensaje = ""
    a = request.POST
    escala = Renta.objects.get(pk=pk)
    tipo = escala.tipo
    escala.hasta = hasta
    escala.save()
    siguientes = Renta.objects.filter(tipo=tipo).order_by('desde')
    choques = []
    nextval = 0
    siguescala = None
    hasta = Decimal(hasta)
    for siguiente in siguientes:
        if siguiente.pk > int(pk):
            if siguiente.hasta < hasta:
                nextval = siguiente.hasta
                siguiente.delete()
    nextescala = nextval + Decimal('0.01')
    try:
        siguescala = Renta.objects.filter(tipo=tipo).get(desde=nextescala)
    except:
        pass
    if siguescala:
        siguescala.desde = hasta + Decimal('0.01')
        siguescala.exceso = hasta
        siguescala.save()
    mensaje = "Tramo Actualizado con Éxito"
    return HttpResponseRedirect('/escalas')
# Finalizan Vistas para Renta

# Inician Vistas para Unidades Organizacionales

class AgregarUnidadOrganizacional(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = UnidadOrganizacional
    fields = ['nombre', 'superseccion']
    template_name = 'planilla/agregar_unidad_organizacional.html'
    success_message = "Unidad Organizacional Agregada con Exito"
    permission_required = 'planilla.add_unidadorganizacional'
    success_url = reverse_lazy('agregar_unidad_organizacional')

    layout = Layout(Fieldset('Agregar Unidad Organizacional: '), Row('nombre','superseccion'))

def unidades(request):
    mensaje = ""
    unidades = UnidadOrganizacional.objects.filter(superseccion__isnull=False)
    return render(request, 'planilla/unidades.html', {'unidades': unidades, 'mensaje': mensaje, })

class ActualizarUnidadOrganizacional(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = UnidadOrganizacional
    fields = ['nombre', 'superseccion']
    template_name = 'planilla/agregar_unidad_organizacional.html'
    success_message = "Unidad Organizacional Modificada con Exito"
    permission_required = 'planilla.change_unidadorganizacional'

    layout = Layout(Fieldset('Modificar Unidad Organizacional: '), Row('nombre', 'superseccion'))

    def get_context_data(self, **kwargs):
        context = super(ActualizarUnidadOrganizacional, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("unidades")

@login_required
def desactivar_unidad(request, pk):
    unidad = UnidadOrganizacional.objects.get(pk=pk)
    if unidad.activo:
        unidad.activo = False
    else:
        unidad.activo = True
    unidad.save()
    messages.add_message(request, messages.INFO, 'Cambio de estado exitoso')

    return HttpResponseRedirect(reverse("unidades"))

# Finalizan Vistas para Unidades Organizacionales

#Inicia vista para informacion de empresa

def actualizar_empresa(request):

    empresa = Empresa.objects.first()
    mensaje = ''
    if request.method == 'POST':
        form = ActualizarEmpresaForm(data=request.POST)
        if empresa:
            form.instance = empresa
        if form.is_valid():
            form.save()
            mensaje = 'Datos actualizados'

    else:
        form = ActualizarEmpresaForm(instance=empresa)


    return render(request, 'planilla/empresa.html', {'form': form, 'mensaje': mensaje})
#Finaliza vista de informacion de empresa

# Inician Vistas para Presupuesto

class AgregarPresupuesto(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = Presupuesto
    fields = ['monto_inicial', 'planilla_anual', 'unidad_organizacional']
    template_name = 'planilla/agregar_presupuesto.html'
    success_message = "Presupuesto Agregado con Exito"
    permission_required = 'planilla.add_presupuesto'
    success_url = reverse_lazy('agregar_presupuesto')

    layout = Layout(Fieldset('Agregar Presupuesto: '), Row('monto_inicial', 'planilla_anual'),Row('unidad_organizacional'))

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.monto_disponible = self.object.monto_inicial
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

def presupuestos(request):
    mensaje = ""
    presupuestos = Presupuesto.objects.all()
    return render(request, 'planilla/presupuestos.html', {'presupuestos': presupuestos, 'mensaje': mensaje, })

class ActualizarPresupuesto(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = Presupuesto
    form_class = PresupuestoForm
    template_name = 'planilla/agregar_presupuesto.html'
    success_message = "Presupuesto Modificado con Exito"
    permission_required = 'planilla.change_presupuesto'

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.monto_disponible = self.object.monto_inicial
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(ActualizarPresupuesto, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("presupuestos")
# Finalizan Vistas para Presupuestos

#Inician vistas de calendarios
@login_required()
def calendarios(request):
    mensaje = ""
    if request.user.has_perm('') or request.user.has_perm(''):
        calendarios = Calendario.objects.all()
        return render(request, 'planilla/calendarios.html', {'calendarios': calendarios, 'mensaje': mensaje, })
    else:
        mensaje = "No posee permisos para acceder a la página solicitada. Para continuar inicie sesión con un usuario privilegiado"
        return render(request, 'autenticacion/login.html', {'mensaje': mensaje, })


def agregar_calendario(request):
    if request.method == 'POST':
        fechas = dict(request.POST)['datepickers']
        print(fechas)
        periocidad = 'M'
        if len(fechas) > 12: periocidad= 'Q'
        año = datetime.datetime.strptime(fechas[0], "%d/%m/%Y").year
        planilla_anual = PlanillaAnual.objects.create(año=año, periocidad_pago=periocidad, devengada=False)
        for fecha in fechas:
            fechaCal = datetime.datetime.strptime(fecha, "%d/%m/%Y")
            planCal = Calendario.objects.create(fecha_pago=fechaCal.date(), activo=False, anio=fechaCal.year, planilla_anual=planilla_anual)

        calendario = Calendario.objects.filter(activo=True).first()
        if not calendario:
            with connection.cursor() as cursor:
                cursor.callproc('iniciar_planilla')

        return HttpResponseRedirect(reverse("calendarios_anuales"))

    else:
        return render(request, 'planilla/agregar_calendario.html', {})


#Finalizan las vistas de calendarios

# Inician vistas para Puesto de Trabajo
class AgregarPuesto(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = PuestoTrabajo
    fields = ['nombre','salario_min', 'salario_max']
    template_name = 'planilla/agregar_puesto.html'
    success_message = "Puesto de Trabajo Agregado con Éxito"
    permission_required = 'planilla.add_puesto'
    success_url = reverse_lazy('agregar_puesto')

    layout = Layout(Fieldset('Agregar Puesto de Trabajo: '), Row('nombre', 'salario_min','salario_max'))

@login_required
def puestos(request):
    mensaje = ""
    if request.user.has_perm('planilla.change_puesto') or request.user.has_perm('planilla.delete_puesto'):
        puestos = PuestoTrabajo.objects.all()
        return render(request, 'planilla/puesto_trabajo.html', {'puestos': puestos, 'mensaje': mensaje, })
    else:
        mensaje = "No posee permisos para acceder a la página solicitada. Para continuar inicie sesión con un usuario privilegiado"
        return render(request, 'autenticacion/login.html', {'mensaje': mensaje, })

class ActualizarPuesto(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = PuestoTrabajo
    fields = ['nombre','salario_min', 'salario_max']
    template_name = 'planilla/agregar_puesto.html'
    success_message = "Puesto de Trabajo Modificado con Éxito "
    permission_required = 'planilla.change_puesto'

    layout = Layout(Fieldset('Agregar Puesto de Trabajo: '), Row('nombre', 'salario_min', 'salario_max'))

    def get_context_data(self, **kwargs):
        context = super(ActualizarPuesto, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("puestos")

@login_required
def desactivar_puesto(request, pk):
    puesto = PuestoTrabajo.objects.get(pk=pk)
    if puesto.activo:
        puesto.activo = False
    else:
        puesto.activo = True
    puesto.save()
    messages.add_message(request, messages.INFO, 'Cambio de estado exitoso')

    return HttpResponseRedirect(reverse("puestos"))
# Finalizan vistas para Puesto de Trabajo


#Inician las vistas de planilla

@login_required()
def ver_planilla(self):
    # calendario = Calendario.objects.filter(fecha_pago__gt=datetime.datetime.today()).order_by('fecha_pago').first()
    calendario = Calendario.objects.filter(activo=True).first()
    if calendario:
        empleados = DetallePlanilla.objects.filter(calendario=calendario.id).values()
        cotizaciones = Cotizacion.objects.all().values()
        ingresos = TipoIngreso.objects.all().values()
        descuentos = DescuentoPersonal.objects.all().values()
        comisiones = Comision.objects.all().values()

        if not empleados:
            with connection.cursor() as cursor:
                cursor.callproc('iniciar_planilla')
                columns = [col[0] for col in cursor.description]
                empleados = [
                    dict(zip(columns, row))
                    for row in cursor.fetchall()
                ]

        return render(self, 'planilla/planilla.html', {'empleados': empleados, 'cotizaciones': cotizaciones, 'ingresos': ingresos, 'descuentos': descuentos, 'comisiones':comisiones})
    else:
        # return render(self, 'planilla/planilla.html')
        return HttpResponseRedirect(reverse("agregar_calendario"))

def obtener_montos(request):
    tipo = int(request.GET.get("tipo"))
    id_detalle = int(request.GET.get("codigo"))

    montos = list
    if tipo==1:
        montos = list(MontoDescuento.objects.filter(detalle_planilla_id=id_detalle).values())
    elif tipo==2:
        montos = list(MontoCotizacion.objects.filter(detalle_planilla_id=id_detalle).values())
    else:
        montos = list(MontoIngreso.objects.filter(detalle_planilla_id=id_detalle).values())


    return JsonResponse({ "montos": montos})


def guardar_comisiones(request):

    if request.method == "POST" and request.is_ajax():
        ventas = Decimal(request.POST.get("ventas"))

        comision = Comision.objects.filter(lim_inferior__lte=ventas, lim_superior__gte=ventas).first()

        if comision:
            monto_comision = ventas * (comision.porcentaje / 100)
            detalle_plan = DetallePlanilla.objects.get(pk=int(request.POST.get("empleado")))
            detalle_plan.ventas = ventas
            detalle_plan.comision = monto_comision
            total = MontoIngreso.objects.filter(detalle_planilla=detalle_plan, tipo_ingreso__gravado='1').aggregate(Sum('monto'))['monto__sum']
            if total is None:
                total = Decimal(0.0)
            gravados = total + monto_comision

            recalcular_cotizaciones(detalle_plan, gravados)
            detalle_plan.actualizar_retenciones()
            detalle_plan.actualizar_total()

            detalle_plan.save()

            return JsonResponse({"total_comision": monto_comision, "total_pagar": detalle_plan.total_pagar, "total_retenciones": detalle_plan.total_retenciones})
        else:
            return JsonResponse({"message": "No hay comision asignada para el rango de ventas"})

    else:
        return JsonResponse({"message": "Consulta sin autorización"})

def guardar_descuentos(request):
    if request.method == "POST" and request.is_ajax():
        datos = json.loads(request.POST.get("datos"))

        id_detalle = int(datos['empleado'])
        detalle = DetallePlanilla.objects.get(pk=id_detalle)
        total_descuentos = 0
        for i in datos['descuentos']:
            monto = float(i['cantidad'])
            id_descuento = int(i['id'])

            MontoDescuento.objects.update_or_create(descuento_personal_id=id_descuento, detalle_planilla_id=id_detalle,
                                                    defaults={'monto': monto})
            total_descuentos = total_descuentos + monto

        detalle.total_descuentos = Decimal(total_descuentos)


        detalle.actualizar_total()
        detalle.save()

        return JsonResponse({"total_descuentos": total_descuentos, "total_pagar": detalle.total_pagar, "id_detalle": id_detalle, "total_retenciones":detalle.total_retenciones})

    else:
        return JsonResponse({"message": "Consulta sin autorización"})



def guardar_ingresos(request):
    if request.method == "POST" and request.is_ajax():
        datos = json.loads(request.POST.get("datos"))

        id_detalle = int(datos['empleado'])
        detalle = DetallePlanilla.objects.get(pk=id_detalle)
        total_ingresos = 0
        gravados = detalle.comision
        if gravados is None:
            gravados = Decimal(0.00)
        for i in datos['ingresos']:
            monto = float(i['cantidad'])
            id_ingreso = int(i['ingreso'])
            # Si es gravado o no viene en i['tipo'], viene el numero que esta en la base
            MontoIngreso.objects.update_or_create(tipo_ingreso_id=id_ingreso, detalle_planilla_id=id_detalle, defaults={'monto':monto})
            total_ingresos = total_ingresos + monto

            if i['tipo'] == '1':
                gravados = gravados + Decimal(monto)

        detalle.total_ingresos = Decimal(total_ingresos)

        recalcular_cotizaciones(detalle, gravados)
        detalle.actualizar_retenciones()
        detalle.actualizar_total()

        detalle.save()

        return JsonResponse({"total_ingresos": total_ingresos, "total_pagar": detalle.total_pagar, "id_detalle": id_detalle, "total_retenciones":detalle.total_retenciones})

    else:
        return JsonResponse({"message": "Consulta sin autorización"})


def recalcular_cotizaciones(detalle, gravados):
    nominal = detalle.salario_base + Decimal(gravados)
    cot_isss = Cotizacion.objects.get(nombre='isss')
    cot_afp = Cotizacion.objects.get(nombre='afp')
    periocidad = PlanillaAnual.objects.get(año=datetime.datetime.today().year)
    per = 2
    nuevo_isss = 0
    nuevo_afp = 0
    nueva_renta = 0

    # Calculo del ISSS
    if nominal <= Decimal(1000):
        nuevo_isss = nominal*cot_isss.porcentaje/100
    else:
        nuevo_isss = Decimal(30.00)

    # Calculo de AFP
    if nominal <= Decimal(5274.52):
        nuevo_afp = nominal*cot_afp.porcentaje/100
    else:
        nuevo_afp = Decimal(5274.52)*cot_afp.porcentaje/100

    if periocidad == 'Q':
        per = 1
        nuevo_isss=nuevo_isss/2
        nuevo_afp=nuevo_afp/2

    MontoCotizacion.objects.filter(cotizacion_id=cot_isss.pk, detalle_planilla_id=detalle.pk).update(monto=nuevo_isss)
    MontoCotizacion.objects.filter(cotizacion_id=cot_afp.pk, detalle_planilla_id=detalle.pk).update(monto=nuevo_afp)
    p_renta = detalle.salario_base + Decimal(gravados) - nuevo_afp - nuevo_isss
    tramo = Renta.objects.filter(desde__lt=p_renta, hasta__gt=p_renta, tipo=per).first()
    nueva_renta = (p_renta - tramo.exceso) * (tramo.porcentaje/100) + tramo.cuota_fija

    detalle.renta = nueva_renta
    detalle.save()
    return 0

def cerrar_planilla(request):
    if request.method == "POST" and request.is_ajax():
        try:
            with connection.cursor() as cursor:
                cursor.callproc('cerrar_planilla')
            return JsonResponse({"mensaje": "La planilla se cerro correctamente"}, status=200)
        except Exception as e:
            mensaje_error = e.args[0].split('\n')[0]
            return JsonResponse({"mensaje": mensaje_error}, status=400)

    else:
        return JsonResponse({"message": "Consulta sin autorización"})


class ImprimirBoletaPDFView(View):
    template_name = 'planilla/imprimir_boleta.html'

    def get(self, request, *args, **kwargs):
        calendario = Calendario.objects.get(activo=True)
        detalles = DetallePlanilla.objects.filter(calendario=calendario.id)
        empresa = Empresa.objects.first()
        detalle_list = []
        for detalle in detalles:
            montos_ingreso = MontoIngreso.objects.filter(detalle_planilla=detalle)
            montos_cotizacion = MontoCotizacion.objects.filter(detalle_planilla=detalle)
            montos_descuento = MontoDescuento.objects.filter(detalle_planilla=detalle)
            detalle_list.append({'detalle': detalle, 'montos_ingreso': montos_ingreso, 'montos_cotizacion': montos_cotizacion, 'montos_descuento': montos_descuento})

        context = {
            'detalle_list': detalle_list,
            'empresa':empresa,
        }

        response = PDFTemplateResponse(request=request, template=self.template_name, filename="r.pdf",
                                       context=context,
                                       show_content_in_browser=True, cmd_options={
                'margin-top': 10,
                'zoom': 1,
                'viewport-size': '1366 x 513',
                'javascript-delay': 1000,
                'footer-center': '[page]/[topage]',
                'no-stop-slow-scripts': True
            },
                                       )
        return response

#Finalizan las vistas de planilla

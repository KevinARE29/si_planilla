from django.conf.urls import url
from .views import *


urlpatterns = [
    url('^$', Inicio.as_view(), name='home'),
    # Inician urls para Estado Civil
    url(r'^agregar_estadocivil/', AgregarEstadoCivil.as_view(), name='agregar_estadocivil'),
    url(r'^estadosciviles/$', estadosciviles, name='estadosciviles'),
    url(r'^actualizar_estadocivil/(?P<pk>\d+)$', ActualizarEstadoCivil.as_view(), name='actualizar_estadocivil'),
    url(r'^desactivar_estadocivil/(?P<pk>\d+)$', desactivar_estadocivil, name='desactivar_estadocivil'),
    # Finalizan urls para Estado Civil

    #Inician urls para Estructura Territorial
    url(r'^estructura_territorial/$', Estructura_territorial, name="estructuras"),
    url(r'^estructura_territorial/ajax/obtener_divisiones/$', obtener_division, name='obtener_divisiones'),
    url(r'^estructura_territorial/ajax/guardar_division/$', guardar_division, name='guardar_division'),
    url(r'^estructura_territorial/ajax/eliminar_division/$', eliminar_division, name='eliminar_division'),
    #Finalizan urls para Estructura Territorial

    # Inician urls para Profesión
    url(r'^agregar_profesion/', AgregarProfesion.as_view(), name='agregar_profesion'),
    url(r'^profesiones/$', profesiones, name='profesiones'),
    url(r'^actualizar_profesion/(?P<pk>\d+)$', ActualizarProfesion.as_view(), name='actualizar_profesion'),
    url(r'^desactivar_profesion/(?P<pk>\d+)$', desactivar_profesion, name='desactivar_profesion'),
    # Finalizan urls para Profesión

    # Inician urls para Comisiones
    url(r'^agregar_comision/', AgregarComision.as_view(), name='agregar_comision'),
    url(r'^agregar_comisiones/$', AgregarComisiones, name='agregar_comisiones'),
    url(r'^actualizar_comisiones/(?P<pk>\d+)$', ActualizarComisiones, name='actualizar_comisiones'),
    url(r'^eliminar_comisiones/(?P<pk>\d+)/(?P<hasta>\d+\.\d{2})$', eliminar_comisiones, name='eliminar_comisiones'),
    url(r'^comisiones/$', comisiones, name='comisiones'),
    url(r'^actualizar_comision/(?P<pk>\d+)$', ActualizarComision.as_view(), name='actualizar_comision'),
    url(r'^eliminar_comision/(?P<pk>\d+)$', eliminar_comision, name='eliminar_comision'),
    # Finalizan urls para Comisiones

    # Inician urls para Tipos de Ingreso
    url(r'^agregar_tipoingreso/', AgregarTipoIngreso.as_view(), name='agregar_tipoingreso'),
    url(r'^ingresos/$', ingresos, name='ingresos'),
    url(r'^actualizar_tipoingreso/(?P<pk>\d+)$', ActualizarTipoIngreso.as_view(), name='actualizar_tipoingreso'),
    url(r'^desactivar_tipoingreso/(?P<pk>\d+)$', desactivar_tipoingreso, name='desactivar_tipoingreso'),
    # Finalizan urls para Tipos de Ingreso

    # Inician urls para Género
    url(r'^agregar_genero/', AgregarGenero.as_view(), name='agregar_genero'),
    url(r'^generos/$', generos, name='generos'),
    url(r'^actualizar_genero/(?P<pk>\d+)$', ActualizarGenero.as_view(), name='actualizar_genero'),
    url(r'^desactivar_genero/(?P<pk>\d+)$', desactivar_genero, name='desactivar_genero'),
    # Finalizan urls para Género

    # Inician urls para Empleado
    url(r'^agregar_empleado/', AgregarEmpleado.as_view(), name='agregar_empleado'),
    url(r'^empleados/$', empleados, name='empleados'),
    url(r'^subordinados/(?P<id>\d+)$', subordinados, name='subordinados'),
    url(r'^actualizar_empleado/(?P<pk>\d+)$', ActualizarEmpleado.as_view(), name='actualizar_empleado'),
    url(r'^desactivar_empleado/(?P<pk>\d+)$', desactivar_empleado, name='desactivar_empleado'),
    # Finalizan urls para Empleado

    # Inician urls para Descuento
    url(r'^agregar_descuento/', AgregarDescuento.as_view(), name='agregar_descuento'),
    url(r'^descuentos/$', descuentos, name='descuentos'),
    url(r'^actualizar_descuento/(?P<pk>\d+)$', ActualizarDescuento.as_view(), name='actualizar_descuento'),
    url(r'^desactivar_descuento/(?P<pk>\d+)$', desactivar_descuento, name='desactivar_descuento'),
    # Finalizan urls para Descuento

    # Inician urls para Cotización
    url(r'^agregar_cotizacion/', AgregarCotizacion.as_view(), name='agregar_cotizacion'),
    url(r'^cotizaciones/$', cotizaciones, name='cotizaciones'),
    url(r'^actualizar_cotizacion/(?P<pk>\d+)$', ActualizarCotizacion.as_view(), name='actualizar_cotizacion'),
    url(r'^desactivar_cotizacion/(?P<pk>\d+)$', desactivar_cotizacion, name='desactivar_cotizacion'),
    # Finalizan urls para Cotización

    # Inician urls para Cotización
    url(r'^actualizar_empresa/', actualizar_empresa, name='actualizar_empresa'),
    # Finalizan urls para Cotización

    # Inician urls para Renta
    url(r'^agregar_escala/$', AgregarEscala, name='agregar_escala'),
    url(r'^escalas/$', escalas, name='escalas'),
    url(r'^actualizar_escala/(?P<pk>\d+)$', ActualizarEscala, name='actualizar_escala'),
    url(r'^eliminar_escala/(?P<pk>\d+)/(?P<hasta>\d+\.\d{2})$', eliminar_tramos, name='eliminar_escala'),
    # Finalizan urls para Renta

    # Inicia urls para Unidades Organizacionales
    url(r'^agregar_unidad_organizacional/$', AgregarUnidadOrganizacional.as_view(), name='agregar_unidad_organizacional'),
    url(r'^unidades/$', unidades, name='unidades'),
    url(r'^actualizar_unidadorganizacional/(?P<pk>\d+)$', ActualizarUnidadOrganizacional.as_view(), name='actualizar_unidadorganizacional'),
    url(r'^desactivar_unidad/(?P<pk>\d+)$', desactivar_unidad, name='desactivar_unidad'),
    # Finalizan urls para Unidades Organizacionales

    #Inicia urls para Calendario
    url(r'^agregar_calendario/', agregar_calendario, name='agregar_calendario'),
    url(r'^calendarios/$', calendarios,name='calendarios_anuales'),
    #Finaliza urls para Calendario

    #Inicial urls para Presupuesto
    url(r'^agregar_presupuesto/', AgregarPresupuesto.as_view(), name='agregar_presupuesto'),
    url(r'^presupuestos/$', presupuestos, name='presupuestos'),
    url(r'^actualizar_presupuesto/(?P<pk>\d+)$', ActualizarPresupuesto.as_view(), name='actualizar_presupuesto'),
    #Finalizan urls para Presupuesto

    # Inician urls para Puesto de Trabajo
    url(r'^agregar_puesto/', AgregarPuesto.as_view(), name='agregar_puesto'),
    url(r'^puesto_trabajo/$', puestos, name='puestos'),
    url(r'^actualizar_puesto/(?P<pk>\d+)$', ActualizarPuesto.as_view(), name='actualizar_puesto'),
    url(r'^desactivar_puesto/(?P<pk>\d+)$', desactivar_puesto, name='desactivar_puesto'),
    # Finalizan urls para Puesto de Trabajo

    #Inicial urls para Planilla
    url(r'^planilla/$',ver_planilla, name='ver_planilla'),
    url(r'^planilla/guardar_ingresos/$',guardar_ingresos, name='guardar_ingreso'),
    url(r'^planilla/guardar_descuentos/$', guardar_descuentos, name='guardar_descuento'),
    url(r'^planilla/guardar_comisiones/$', guardar_comisiones, name='guardar_comisiones'),
    url(r'^planilla/obtener_montos/$', obtener_montos, name='obtener_montos'),
    url(r'^planilla/cerrar_planilla/$', cerrar_planilla, name='cerrar_planilla'),
    url(r'^planilla/imprimir_boleta/$', ImprimirBoletaPDFView.as_view(), name='imprimir_boleta'),
    #Finalizan urls para Planilla
]
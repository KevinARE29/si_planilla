from django import forms
from .models import Cotizacion, Renta, Empresa, Presupuesto, CHOICES, Comision
from material import *
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q

class CotizacionForm(forms.ModelForm):
    class Meta:
        model = Cotizacion
        fields = ['nombre', 'porcentaje']

    layout = Layout(Fieldset('Actualizar Profesion: '), Row('nombre', 'porcentaje'))

    def __init__(self, *args, **kwargs):
        super(CotizacionForm, self).__init__(*args, **kwargs)
        self.fields['porcentaje'].help_text = 'Valor entre 0 y 100, con dos decimales de precisión.'

    def clean_nombre(self):
        nombre = self.cleaned_data['nombre'].lower()
        cotizaciones = Cotizacion.objects.all()
        for cotizacion in cotizaciones:
            if cotizacion.nombre.lower() == nombre:
                raise forms.ValidationError("¡Ya existe una cotización con ese nombre!")
        return nombre

# Inician Forms de Renta

class AgregarEscalaForm(forms.ModelForm):
    class Meta:
        model = Renta
        fields = ['desde', 'hasta', 'porcentaje', 'cuota_fija', 'exceso', 'tipo']

    layout = Layout(Fieldset('Agregar Tramo de Renta: '), Row('desde', 'hasta', 'tipo'),
                    Row('porcentaje', 'exceso', 'cuota_fija'))

    def __init__(self, *args, **kwargs):
        super(AgregarEscalaForm, self).__init__(*args, **kwargs)
        self.fields['desde'].widget.attrs['readonly'] = True
        self.fields['exceso'].widget.attrs['readonly'] = True
# Finalizan Forms de Renta

# Inician Forms de Comisiones

class AgregarComisionForm(forms.ModelForm):
    class Meta:
        model = Comision
        fields = ['lim_inferior', 'lim_superior', 'porcentaje']

    layout = Layout(Fieldset('Agregar Comision: '), Row('lim_inferior', 'lim_superior'),
                    Row('porcentaje'))

    def __init__(self, *args, **kwargs):
        super(AgregarComisionForm, self).__init__(*args, **kwargs)
        self.fields['lim_inferior'].widget.attrs['readonly'] = True
# Finalizan Forms de Comisiones

#Inicia form de empresa

class ActualizarEmpresaForm(forms.ModelForm):
    class Meta:
        model = Empresa
        fields = ['nombre', 'nit', 'nic', 'telefono', 'webpage', 'email', 'representante_legal']

    layout = Layout(Fieldset('Agregar informacion de la empresa'), Row('nombre', 'nit', 'nic'),
                    Row('telefono', 'webpage', 'email'), Row('representante_legal'))

# Finaliza form de empesa

# Inician Forms de Presupuesto
class PresupuestoForm(forms.ModelForm):
    class Meta:
        model = Presupuesto
        fields = ['monto_inicial', 'unidad_organizacional', 'planilla_anual']

    layout = Layout(Fieldset('Modificar Presupuesto: '), Row('monto_inicial', 'planilla_anual'),
                    Row('unidad_organizacional'))

    def clean(self):
        cleaned_data = super(PresupuestoForm, self).clean()
        p = Presupuesto.objects.get(pk=self.instance.id)
        if p.monto_disponible != p.monto_inicial:
            raise ValidationError(
                {'monto_inicial': _('Este presupuesto cuenta con descuentos de planilla y no se puede modificar'), }
            )
# Finalizan Forms de Renta
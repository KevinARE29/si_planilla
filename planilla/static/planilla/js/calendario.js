function crearForm() {
    var agnio = document.getElementById("periodo").value;
    var opcion = document.getElementById("opciones").value;
    if(!agnio && opcion != 0) {
        alert('No se ha elegido el año de calendario');
    }
    else {

        limpiar();
        if(opcion == 1) {
            crearMensual();
        }
        else if(opcion == 2) {
            crearQuincenal();
        }
    }
}

function reiniciar() {
    var select = document.getElementById("opciones").value = 0;
    limpiar();
}

function limpiar() {
    $('#fechasDiv').remove()
    $('#customud').remove()
}

function crearMensual() {
    var agnio = document.getElementById("periodo").value;
    var meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
    // form = document.getElementById('formCal')
    // var form = $("<form>", {
    //         id: 'formMeses',
    //         class: 'col s10'
    // });
    var divFechas = $("<div>", {
            id: 'fechasDiv'
        });

    for(var i = 0; i <= 11; i++) {

        var fechaI = new Date(agnio, i, 1);
        var fechaF = new Date(agnio, i+1, 0);

        var dtpoptions = {
                format: "dd/mm/yyyy",
                maxViewMode: 1,
                startDate: fechaI,
                endDate: fechaF,
                language: "es",
                daysOfWeekDisabled: [0, 6],
                autoclose: true
        };

        var div = $("<div>", {
            id: meses[i] + 'R',
            class: 'row'
        });
        var div2 = $("<div>", {
            id: meses[i] + 'D',
            class: 'col s5'
        });

        var datep = $("<input readonly data-form-control='date' id='datepicker" + i + "' name='datepickers' type='text'  " +
            "onclick='once(this);' ondblclick='twice(this);' placeholder='dd/mm/yyyy'>");

        var label = $("<label>", {
            text: 'Seleccione fecha de pago para el mes de ' + meses[i],
            for: ''
        });
        div2.append(label);
        div2.append(datep);
        div.append(div2);
        // form.append(div);
        divFechas.append(div)
        $(datep).datepicker(dtpoptions);
    }

    var pd = $("<div>", {
        id: 'pd',
        class: 'col s4'
    });
    var ud = $("<div>", {
        id: 'customud',
        class: 'row'
    });

    ud.append($("<input>", {
            text: 'Guardar',
            type: 'submit',
            class: 'waves-effect waves-light btn'//,
            // onclick: 'guardarCMensual();'
        })
    );

    ud.append(pd);
    // form.append(ud);
    $('#formCal').append(divFechas)
    $('#formCal').append(ud)
    //$("#container").append(form);
}

function crearQuincenal() {
    var agnio = document.getElementById("periodo").value;
    var meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
    // var form = $("<form>", {
    //         id: 'formMeses',
    //         class: 'col s10'
    // });
        var divFechas = $("<div>", {
            id: 'fechasDiv'
        });

    for(var i = 0; i <= 11; i++) {

        var fechaI = new Date(agnio, i, 1);
        var fechaF = new Date(agnio, i+1, 0);

        var dtpoptions = {
                format: "dd/mm/yyyy",
                maxViewMode: 1,
                startDate: fechaI,
                endDate: fechaF,
                daysOfWeekDisabled: [0, 6],
                language: "es",
                autoclose: true
        };

        var div = $("<div>", {
            id: meses[i] + 'R',
            class: 'row'
        });
        var div2 = $("<div>", {
            id: meses[i] + 'D',
            class: 'col s5'
        });

        var datep = $("<input readonly data-form-control='date' id='datepicker" + i + "' name='datepickers' type='text'  " +
            "id='datepicker"+ i +"' onclick='once(this);' ondblclick='twice(this);' placeholder='dd/mm/yyyy'>");

        var label = $("<label>", {
            text: 'Seleccione fecha de pago para la primera semana de ' + meses[i],
            for: 'datepicker' + i + '1'
        });
        var datep1 = $("<input readonly data-form-control='date' id='datepicker" + i + "' name='datepickers' type='text'  " +
            "id='datepicker"+ i +"' onclick='once(this);' ondblclick='twice(this);' placeholder='dd/mm/yyyy'>");

        var label1 = $("<label>", {
            text: 'Seleccione fecha de pago para la segunda semana de ' + meses[i],
            for: 'datepicker' + i + '2'
        });
        div2.append(label);
        div2.append(datep);
        div2.append(label1);
        div2.append(datep1);
        div.append(div2);
        // form.append(div);
        divFechas.append(div)
        $(datep).datepicker(dtpoptions);
        $(datep1).datepicker(dtpoptions);
    }


    var pd = $("<div>", {
        id: 'pd',
        class: 'col s4'
    });
    var ud = $("<div>", {
        id: 'customud',
        class: 'row'
    });

    ud.append($("<a>", {
            text: 'Guardar',
            type: 'submit',
            class: 'waves-effect waves-light btn',
            onclick: 'guardarCQuincenal();'
        })
    );

    ud.append(pd);
    // form.append(ud);
    $('#formCal').append(divFechas)
    $('#formCal').append(ud)
    //$("#container").append(form);
}

function once(dtp){
    $(dtp).datepicker('hide');
}

function twice(dtp) {
    $(dtp).datepicker('show');
}

function guardarCMensual() {
    var dates = document.getElementsByName("datepickers");
    postDates = []
    for(var i = 0; i <= 11; i++) {
        console.log(dates[i].value);
        postDates.push(dates[i].value)
    }

    $.ajax({
      type: "POST",
      url: '/',
      data: postDates,
      success: function () {
          console.log('Done')
      }
    });
}

function guardarCQuincenal() {
    var dates = document.getElementsByName("datepickers");
    for(var i = 0; i <= 23; i++) {
        console.log(dates[i].value);
    }
}

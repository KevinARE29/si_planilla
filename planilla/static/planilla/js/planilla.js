
$(document).ready(function() {
     $(".target").keydown(function (event) {
         var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
         var val = $(this).attr("id");
         val = $("input[id='" + val + "']").val() + key;
         /*if( (event.keyCode == 190) || (event.keyCode == 8)) {
             return true;
         }else {
             if(!expFormato.test(val)) {
                 event.preventDefault();
                 return false;
             }
         }*/
         if(event.keyCode == 189) {
             event.preventDefault();
             return false;
         }
     });
});

function mostrarIngresos(sender) {
    $(".target").val("");
    id = (sender.parentNode.parentNode).getAttribute("id");
    var codigos = $("p[name^='codigo']");
    codigo = codigos[id].textContent;
    $.ajax({
        url: 'obtener_montos/',
        type:'GET',
        data: {
            codigo: codigo,
            tipo: 0 //Hay un switch en views que decide que tipo de monto equivale a ingreso, descuento o retenciones
        },
        dataType: 'json',
        success: function(data) {
            for(var i = 0; i < todosIngresos.length; i++) {
                for(var j = 0; j < data.montos.length; j++) {
                    if(todosIngresos[i].id == data.montos[j].tipo_ingreso_id) {
                        $("input[id='" + todosIngresos[i].nombre + "']").val(parseFloat(data.montos[j].monto).toFixed(2));
                    }
                }
            }
        },
        error: function(msg) {
            M.toast({html: 'Error 500', classes:'rounded'});
        }
    });
}

function mostrarDescuentos(sender) {
    $(".target").val("");
    id = (sender.parentNode.parentNode).getAttribute("id");
    var codigos = $("p[name^='codigo']");
    codigo = codigos[id].textContent;
    $.ajax({
        url: 'obtener_montos/',
        type:'GET',
        data: {
            codigo: codigo,
            tipo: 1
        },
        dataType: 'json',
        success: function(data) {
            for(var i = 0; i < todosDescuentos.length; i ++) {
                for(var j = 0; j < data.montos.length; j++) {
                    if(todosDescuentos[i].id == data.montos[j].descuento_personal_id) {
                        $("input[id='" + todosDescuentos[i].nombre + "']").val(data.montos[j].monto);
                    }
                }
            }
        },
        error: function(msg) {
            M.toast({html: 'Error 500', classes: 'rounded'});
        }
    });
}

function mostrarComisiones(sender) {
    $(".target").val("");
    id = (sender.parentNode.parentNode).getAttribute("id");
    var codigos = $("p[name^='codigo']");
    codigo = codigos[id].textContent;
    var venta = todosEmpleados[id].ventas;
    $("#venta").val(venta);
}

function mostrarRetenciones(sender) {
    $(".target").val("");
    id = (sender.parentNode.parentNode).getAttribute("id");
    var codigos = $("p[name^='codigo']");
    codigo = codigos[id].textContent;
    $.ajax({
        url: 'obtener_montos/',
        type:'GET',
        data: {
            codigo: codigo,
            tipo: 2
        },
        dataType: 'json',
        success: function(data) {
            $("#renta").val(todosEmpleados[id].renta);
            for(var i = 0; i < todosCotizaciones.length; i++) {
                for(var j = 0; j < data.montos.length; j++) {
                    if(todosCotizaciones[i].id == data.montos[j].cotizacion_id) {
                        $("input[id='" + todosCotizaciones[i].nombre + "']").val(data.montos[j].monto);
                    }
                }
            }
        },
        error: function(msg) {
            M.toast({html: 'Error 500', classes: 'rounded'});
        }
    });
}

function guardarIngresos() {
    var suma = 0;
    var ings = $("input[name^='ing']");
    var datos = {};
    datos.ingresos = [];
    var valor = 0;
    datos.empleado = codigo
    for(var i = 0; i < ings.length; i++) {
        if(todosIngresos[i].activo) {
            valor = isNaN(parseFloat(ings[i].value)) ? 0.00 : parseFloat(ings[i].value).toFixed(2);
            suma += parseFloat(valor);
            datos.ingresos.push({
                tipo: todosIngresos[i].gravado,
                ingreso: todosIngresos[i].id,
                nombre: todosIngresos[i].nombre,
                cantidad: valor
            });
        }
    }
    var ingresos = $("p[name^='ingreso']");
    datos = JSON.stringify(datos);
    $.ajax({
        url: 'guardar_ingresos/',
        type: 'POST',
        dataType: 'json',
        data: {datos: datos},
        success: function(response) {
            var m = M.Modal.getInstance($("#modalIngresos"));
            var ingresos = $("p[name^='ingreso']");
            var totales = $("p[name^='total']");
            var retenciones = $("p[name^='retencion']");
            totales[id].textContent = "$" + parseFloat(response.total_pagar).toFixed(2);
            ingresos[id].textContent = "$" + suma.toFixed(2);
            retenciones[id].textContent = "$" + parseFloat(response.total_retenciones).toFixed(2);
            m.close();
        },
        error: function(msg) {
            M.toast({html: 'Error 500', classes:'rounded'});
        }
    });
}

function guardarDescuentos() {
    var suma = 0;
    var descs = $("input[name^='descs']");
    var datos = {};
    var dcounter = 0;
    datos.descuentos = [];
    datos.empleado = codigo
    var descdias = isNaN(parseFloat($("input[id='dias inasistidos']").val())) ? 0.00 : parseFloat($("input[id='dias inasistidos']").val()).toFixed(2);
    for(var i = 0; i < todosDescuentos.length; i++) {
        if(todosDescuentos[i].activo) {
            if(todosDescuentos[i].nombre == "dias inasistidos") {
                suma += parseFloat(descdias);
                datos.descuentos.push({
                    id: todosDescuentos[i].id,
                    tipo: todosDescuentos[i].activo,
                    nombre: todosDescuentos[i].nombre,
                    cantidad: isNaN(parseFloat(descdias)) ? 0.00 : parseFloat(descdias).toFixed(2)
                });
            }
            else {
                var valor = isNaN(parseFloat(descs[i].value)) ? 0.00 : parseFloat(descs[i].value).toFixed(2);
                suma += parseFloat(valor);
                datos.descuentos.push({
                    id: todosDescuentos[i].id,
                    tipo: todosDescuentos[i].activo,
                    nombre: todosDescuentos[i].nombre,
                    cantidad: valor
                });
                dcounter++;
            }
        }
    }
    var descuentos = $("p[name^='descuento']");
    datos = JSON.stringify(datos)
    $.ajax({
        url: 'guardar_descuentos/',
        type: 'POST',
        dataType: 'json',
        data: {"datos": datos},
        success: function(response) {
            var m = M.Modal.getInstance($("#modalDescuentos"));
            var descuentos = $("p[name^='descuento']");
            var retenciones = $("p[name^='retencion']");
            var totales = $("p[name^='total']");
            descuentos[id].textContent = "$" + suma.toFixed(2);
            retenciones[id].textContent = "$" + parseFloat(response.total_retenciones).toFixed(2);
            totales[id].textContent = "$" + parseFloat(response.total_pagar).toFixed(2);
            m.close();
        },
        error: function(msg) {
            M.toast({html: 'Error 500', classes: 'rounded'});
        }
    });
}

function guardarComisiones() {
    var venta = isNaN(parseFloat($("#venta").val())) ? 0.00 : parseFloat($("#venta").val()).toFixed(2);
    var flag = false;
    var datos = [];
    datos = {empleado: codigo, ventas: venta};
    $.ajax({
        url: 'guardar_comisiones/',
        type: 'POST',
        dataType: 'json',
        data: datos,
        success: function(data) {
            console.log(data);
            var m = M.Modal.getInstance($("#modalComisiones"));
            var comisiones = $("p[name^='comision']");
            var ti = data.total_comision;
            comisiones[id].textContent = "$" + parseFloat(ti).toFixed(2);
            var totales = $("p[name^='total']");
            var retenciones = $("p[name^='retencion']");
            todosEmpleados[id].ventas = parseFloat(venta).toFixed(2);
            retenciones[id].textContent = "$" + parseFloat(data.total_retenciones).toFixed(2);
            totales[id].textContent = "$" + parseFloat(data.total_pagar).toFixed(2);
            m.close();
        },
        error: function(msg) {
            M.toast({html: 'Error 500', classes:'rounded'});
        }
    });
}

function cerrarPlanilla() {
    $.ajax({
        url: 'cerrar_planilla/',
        type: 'POST',
        dataType: 'json',
        data: "",
        success: function(msg) {
            M.toast({html: msg.mensaje, classes:'rounded'});
            $(location).attr('href', '/calendarios/')
        },
        error: function(msg) {
            M.toast({html: msg.responseJSON.mensaje, classes:'rounded'});
        }
    });
}

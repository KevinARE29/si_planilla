
//Creación de los formularios según la eleccion en select
function crearForm(){
    var opcion = document.getElementById("opciones").value;
    if(opcion != 0) {
        limpiarArea();
        if(opcion == 1) {
            formPais();
        }
        else if(opcion == 2) {
            formDepartamento();
        }
        else if(opcion == 3){
            formMunicipio();
        }
    }
}

//Creacion de formulario para ingresar o eliminar un país
function formPais() {
    var form = $("<form>", { id:'agregarPais'});
    var title = $("<h6>", { html:'Lista de paises ingresados'});
    var select = $("<select>", { name: 'listaPaises',  id: 'listaPaises', class:'browser-default'});
    var div = $("<div>",{ id:'primerRow', class:'row'});
    var div2 = $("<div>",{ id:'primerDiv', class:'col s6' });
    var div3 = $("<div>",{ id:'segundoRow', class: 'row'});
    var div4 = $("<div>",{ id:'segundoDiv', class: 'col s6'});
    var div5 = $("<div>", { id: 'tercerRow', class: 'row'});
    var div6 = $("<div>", { id: 'tercerDiv', class: 'col s6' });

    form.append(title);

    div2.append(select);
    select.append($("<option>",{value: 0, text:'Paises disponibles'}));

    div.append(div2);
    div.append($("<div>", { class: 'col s4'}).append($("<a>", {
        class:'waves-effect waves-light btn',
        onclick: 'eliminarDivision(1);',
        text: 'Eliminar país'
    })));
    form.append(div);

    form.append($("<label>",{
        text:'Nombre del país a ingresar:'
    }));

    div4.append($("<input>", {
            type: 'text',
            id: 'nombrePais',
            name: 'nombrePais',
            class: 'input-field col s4'
        })
    );

    div3.append(div4);
    form.append(div3);

    div6.append($("<a>",
        {
            id: 'botonPais',
            name: 'botonPais',
            class: 'waves-effect waves-light btn',
            text: 'Guardar',
            onclick: 'guardarDivision(1);'
        })
    );

    div5.append(div6);
    form.append(div5);

    //Se agrega el formulario al div asignado
    $("#container").append(form);
    //Primera consulta para obtener los paises
    obtenerDivision(1);
}

//Creación de formulario para crear o eliminar un departamento
function formDepartamento() {
    var form = $("<form>", { id:'agregarDepartamento'});
    var title = $("<h6>", { html:'Seleccione un país'});
    var selectP = $("<select>", {
        name: 'listaPaises',
        id: 'listaPaises',
        class:'browser-default',
        //Se agrega la función para consultar los departamentos del país seleccionado
        onchange: 'obtenerDivision(2);'
    });
    var selectD = $("<select>", {
        name: 'listaDepartamentos',
        id: 'listaDepartamentos',
        class:'browser-default'
    });
    var div = $("<div>",{ id:'primerRow', class:'row'});
    var div2 = $("<div>",{ id:'primerDiv', class:'col s6' });
    var div3 = $("<div>",{ id:'segundoRow', class: 'row'});
    var div4 = $("<div>",{ id:'segundoDiv', class: 'col s6'});
    var div5 = $("<div>",{ id:'tercerRow', class: 'row'});
    var div6 = $("<div>",{ id:'tercerDiv', class: 'col s6'});
    var div7 = $("<div>",{ id:'cuartoRow', class: 'row'});
    var div8 = $("<div>",{ id:'cuartoDiv', class: 'col s6'});

    form.append(title);
    form.append($("<label>",{
        text: 'Seleccione un país'
    }));

    div2.append($(selectP));
    selectP.append($("<option>",{value: 0, text:'Paises disponibles'}));

    div.append(div2);
    form.append(div);

    form.append($("<label>", {
        text: 'Lista de departamentos ingresados'
    }));

    div4.append($(selectD));
    selectD.append($("<option>",{value: 0, text:'Departamentos disponibles'}));

    div3.append(div4);
    div3.append($("<div>", { class: 'col s4'}).append($("<a>", {
        text: 'ELiminar departamento',
        class: 'waves-effect waves-light btn',
        onclick: 'eliminarDivision(2);'
    })));
    form.append(div3);

    div6.append($("<input>", {
            type: 'text',
            id: 'nombreDepartamento',
            name: 'nombreDepartamento',
            class: 'input-field col s10'
        })
    );

    div6.append($("<label>",{
        for:'name',
        class:'validate',
        type:'text',
        html:'Nombre del departamento'
        })
    );
    div5.append(div6);
    form.append(div5);

    div8.append($("<a>", {
            id: 'botonDepartamento',
            name: 'botonDepartamento',
            class: 'waves-effect waves-light btn',
            html: 'Guardar',
            onclick: 'guardarDivision(2);'
        })
    );

    div7.append(div8);
    form.append(div7);
    //Se agrega el formulario al div designado
    $("#container").append(form);
    //Primera consulta para obtener los paises ingresados
    obtenerDivision(1);
}

//Se crea el formulario para crear o eliminar un municipio
function formMunicipio() {
    var form = $("<form>", { id:'agregarMunicipio'});
    var title = $("<h6>", { html:'Seleccione un país y un departamento'});
    var selectP = $("<select>", {
        name: 'listaPaises',
        id: 'listaPaises',
        class:'browser-default',
        //Se agrega la función para consultar los departamentos del país seleccionado
        onchange: 'obtenerDivision(2);'
    });
    var selectD = $("<select>", {
        name: 'listaDepartamentos',
        id: 'listaDepartamentos',
        class:'browser-default',
        //Se agrega la función para consultar los municipios del departamento seleccionado
        onchange: 'obtenerDivision(3);',
    });
    var selectM = $("<select>", {
        name: 'listaMunicipios',
        id: 'listaMunicipios',
        class:'browser-default'
    });
    var div = $("<div>",{ id:'primerRow', class:'row'});
    var div2 = $("<div>",{ id:'primerDiv', class:'col s6' });
    var div3 = $("<div>",{ id:'segundoRow', class: 'row'});
    var div4 = $("<div>",{ id:'segundoDiv', class: 'col s6'});
    var div5 = $("<div>",{ id:'tercerRow', class: 'row'});
    var div6 = $("<div>",{ id:'tercerDiv', class: 'col s6'});
    var div7 = $("<div>",{ id:'cuartoRow', class: 'row'});
    var div8 = $("<div>",{ id:'cuartoDiv', class: 'col s6'});
    var div9 = $("<div>",{ id:'quintoRow', class: 'row'});
    var div10 = $("<div>",{ id:'quintoDiv', class: 'col s6'});

    form.append(title);
    form.append($("<label>",{
            text: 'Seleccione un país',
            id: 'indicaciones'
        })
    );

    form.append($("<label>", {
        text: 'Seleccione un país disponible'
    }));

    div2.append($(selectP));
    selectP.append($("<option>",{value: 0, text:'Paises disponibles'}));

    div.append(div2);
    form.append(div);

    form.append($("<label>", {
        text: 'Seleccione un departamento disponible'
    }));
    div4.append($(selectD));
    selectD.append($("<option>",{value: 0, text:'Departamentos disponibles'}));

    div3.append(div4);
    form.append(div3);

    form.append($("<label>", {
        text: 'Lista de municipios ingresados'
    }));

    div6.append($(selectM));
    selectM.append($("<option>",{value: 0, text:'Municipios disponibles'}));
    div6.append();

    div5.append(div6);
    div5.append($("<div>", { class:'col s4' }).append($("<a>",{
        text: 'Eliminar municipio',
        onclick: 'eliminarDivision(3)',
        class: 'waves-effect waves-light btn'
    })));

    form.append(div5);

    div8.append($("<input>",
        {
            type: 'text',
            id: 'nombreMunicipio',
            name: 'nombreMunicipio',
            class: 'input-field col s10'
        })
    );
    div8.append($("<label>",{
        class:'validate',
        type:'text',
        for: 'nombreMunicipio',
        html:'Nombre del municipio'
        })
    );

    div7.append(div8);
    form.append(div7);

    div10.append($("<a>", {
            id: 'botonMunicipio',
            name: 'botonMunicipio',
            class: 'waves-effect waves-light btn',
            html: 'Guardar',
            onclick: 'guardarDivision(3);'
        })
    );

    div9.append(div10);
    form.append(div9);

    //Se agrega el formulario al div asignado
    $("#container").append(form);
    //Primera consulta de los países disponibles
    obtenerDivision(1);
}

//Función para eliminar cualquier formulario en div container
function limpiarArea() {
    $("#container").empty();
}

//Función para obtener paises, departamentos y municipios según la variable num
function obtenerDivision(num) {
    var select;
    var superdiv;
    //Asignación de los elementos de cada form según el evento onchange de cada select
    if(num == 1) {
        $("#listaPaises option").remove();
        select = $("select[name='listaPaises']");
        select.append($("<option>",{value: 0, text:'Paises disponibles'}));
        superdiv = 0;
    }
    if(num == 2) {
        $("#listaDepartamentos option").remove();
        select = $("select[name='listaDepartamentos']");
        superdiv = $("select[name='listaPaises']").val();
        select.append($("<option>",{value: 0, text:'Departamentos disponibles'}));
    }
    else if(num == 3) {
        $("#listaMunicipios option").remove();
        select = $("select[name='listaMunicipios']");
        superdiv = $("select[name='listaDepartamentos']").val();
        select.append($("<option>",{value: 0, text:'Municipios disponibles'}));
    }
    //Creación de la consulta
    $.ajax({
        //Ruta de la consulta
        url:'ajax/obtener_divisiones/',
        dataType: 'json',
        type: 'POST',
        data: {
            super: superdiv
        },
        success: function(data) {
            //Se agregan los emenetos de la consulta al respectivo select
            for(var k in data.divisiones) {
                select.append($("<option>", {
                        id: data.divisiones[k].id,
                        value: data.divisiones[k].id,
                        text: data.divisiones[k].nombre
                    })
                );
            }
        }
    });
}

//Función para guardar países, departamentos o municipios
function guardarDivision(num) {
    var nombre;
    var select;
    var selecto;
    var selected;
    var mensaje;
    var mensajedup;
    var flag = false;
    //Asignación de las variables según la variable num
    if(num == 1) {
        nombre = $("#nombrePais").val();
        select = $("select[name='listaPaises']");
        selected = null;
        selecto = $("#listaPaises option");
        mensaje = "País ingresado con éxito";
        mensajedup = "Este país ya está ingresado";
    }
    else if(num == 2) {
        nombre = $("#nombreDepartamento").val();
        select = $("select[name='listaDepartamentos']");
        selected = $("select[name='listaPaises']").val();
        selecto = $("#listaDepartamentos option");
        mensaje = "Departamento ingresado con éxito";
        mensajedup = "Este departamento ya está registrado";
    }
    else if(num == 3) {
        nombre = $("#nombreMunicipio").val();
        select = $("select[name='listaMunicipios']");
        selected = $("select[name='listaDepartamentos']").val();
        selecto = $("#listaMunicipios option");
        mensaje = "Municipio ingresado con éxito";
        mensajedup = "Este municipio ya está registrado";
    }
    //Validación de que se haya escodigo un país o departamento
    if ( (num == 1 && !selected) || !(!selected || selected == 0)){
        if (!nombre) {
            M.toast({html:'Debe ingresar un nombre', classes: 'rounded'});
        }
        else {
            //Validación para evitar divisiones repetidas
            selecto.each(function () {
                if ($(this).text() == nombre) {
                    flag = true;
                }
            });
            if (flag == true) {
                M.toast({html: mensajedup, classes:'rounded'});
            } else {
                //Creación de la consulta
                $.ajax({
                    url: 'ajax/guardar_division/',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        nombre: nombre,
                        super: selected
                    },
                    success: function (data) {
                        M.toast({html: mensaje , classes:'rounded'});
                        //Se agrega el elemento guardado exitosamente a la colección
                        select.append($("<option>", {
                                id: data.id,
                                value: data.id,
                                text: data.nombre
                            })
                        );
                        $(".input-field").val("");
                    },
                    error: function (data) {
                        M.toast({html: 'Sin autorización: ' + data.message, classes: 'rounded'});
                    }
                });
            }
        }
    }
    else {
        M.toast({html: 'Debe elegir donde pertenecerá esta división', classes:'rounded'});
    }
}

function eliminarDivision(num) {
    var select;
    var superdiv;
    var mensaje;
    var mensajee;
    if(num == 1) {
        superdiv = "#listaPaises";
        select = $("#listaPaises").val();
        mensaje = "País eliminado con éxito";
        mensajee = "No ha seleccionado ningún país";
    }
    else if(num == 2) {
        superdiv = "#listaDepartamentos";
        select = $("#listaDepartamentos").val();
        mensaje = "Departamento eliminado con éxito";
        mensajee = "No ha seleccionado ningún departamento";
    }
    else if(num == 3) {
        superdiv = "#listaMunicipios";
        select = $("#listaMunicipios").val();
        mensaje = "Municipio eliminado con éxito";
        mensajee = "No ha seleccionado ningún municipio";
    }
    if(select != 0) {
        $.ajax({
            url: 'ajax/eliminar_division/',
            type: 'POST',
            dataType: 'json',
            data: {
                id: select,
                division: num
            },
            success: function(data) {
                $(superdiv + " option[value='" + select + "']").remove();
                M.toast({html: data.message + ': ' + mensaje, classes: 'rounded'});
            },
            error: function(data) {
                M.toast({html: data.message, classes:'rounded'});
            }
        });
    }
    else {
        M.toast({html: mensajee, classes: 'rounded'});
    }
}

from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.postgres.fields import ArrayField

# Create your models here.
CHOICES = (
    ('1', 'Quincenal'),
    ('2', 'Mensual'),
)

class PlanillaAnual(models.Model):
    año = models.IntegerField('Año', blank=False, null=False, unique=True)
    periocidad_pago = models.CharField('Periocidad de pago', max_length=50, blank=False, null=False)
    devengada = models.BooleanField('¿Está devengada?', default=False)

    def __str__(self):
        return 'Planilla para el año: - %s' % (self.año.__str__())

    class Meta:
        verbose_name = 'PlanillaAnual'
        verbose_name_plural = 'PlanillasAnuales'

class EstadoCivil(models.Model):
    nombre = models.CharField('Nombre', max_length = 64, unique = True)
    activo = models.BooleanField('¿Activo?', default=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Estado Civil'
        verbose_name_plural = 'Estados Civiles'

class Profesion(models.Model):
    nombre = models.CharField('Nombre', max_length = 64, unique = True)
    activo = models.BooleanField('¿Activo?', default=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Profesión'
        verbose_name_plural = 'Profesiones'

class Genero(models.Model):
    nombre = models.CharField('Nombre', max_length = 64, unique=True)
    activo = models.BooleanField('¿Activo?', default=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Género'
        verbose_name_plural = 'Géneros'

class Renta(models.Model):
    tipo = models.CharField('Tipo', max_length = 1, choices = CHOICES, blank = False, null = False, default = ('1'))
    desde = models.DecimalField('Desde', max_digits=8, decimal_places=2, blank=False, null=False, validators = [MinValueValidator(0)])
    hasta = models.DecimalField('Hasta', max_digits=8, decimal_places=2, blank=False, null=False, validators = [MinValueValidator(0)])
    porcentaje = models.DecimalField('Porcentaje a aplicar', max_digits=5, decimal_places=2, blank=True, null=True, validators = [MinValueValidator(0), MaxValueValidator(100.00)])
    exceso = models.DecimalField('Sobre el exceso de', max_digits=8, decimal_places=2, blank=True, null=True, validators = [MinValueValidator(0)])
    cuota_fija = models.DecimalField('Más cuota fija de', max_digits=8, decimal_places=2, blank=True, null=True, validators = [MinValueValidator(0)])

    def __str__(self):
        return 'Tramo de Renta: %.2f - %.2f' % (self.desde, self.hasta)

    def get_tipo_display(self):
        for tipo in CHOICES:
            if self.tipo == tipo[0]:
                return tipo[1]

    class Meta:
        unique_together = (('tipo', 'desde',), ('tipo', 'hasta',))
        verbose_name ='Tabla de Renta'
        verbose_name_plural = 'Tramos de Tabla de Renta'

class Comision(models.Model):
    lim_inferior = models.DecimalField('Límite Inferior', max_digits=8, decimal_places=2, blank=False, null=False, validators=[MinValueValidator(0)])
    lim_superior = models.DecimalField('Límite Superior', max_digits=8, decimal_places=2, blank=False, null=False, validators=[MinValueValidator(0)])
    porcentaje = models.DecimalField('Tasa de Comisión', max_digits=5, decimal_places=2, blank=False, null=False, validators=[MinValueValidator(0), MaxValueValidator(100.00)])

    def __str__(self):
        return 'Tasa de Comisión: %f - %f' % (self.lim_inferior, self.lim_superior)

    class Meta:
        verbose_name = 'Tasa de Comisión'
        verbose_name_plural = 'Tasas de Comisión'

class UnidadOrganizacional(models.Model):
    nombre = models.CharField('Nombre', max_length=128, unique=True)
    activo = models.BooleanField('¿Unidad Organizacional Activa?', default=True)
    superseccion = models.ForeignKey('self', null=True, blank=True, related_name='subsecciones')

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Unidad Organizacional'
        verbose_name_plural = 'Unidades Organizacionales'

class Calendario(models.Model):
    anio = models.IntegerField('Año', blank=False, null=False, unique=False)
    fecha_pago = models.DateField('Fecha de Pago', blank=False, null=False)
    activo = models.BooleanField('¿Calendario Activo?', default=True)
    planilla_anual = models.ForeignKey(PlanillaAnual, null=False, default=1)

    def __str__(self):
        return str(self.anio)

    def get_periodicidad_display(self):
        for periodicidad in CHOICES:
            if self.periodicidad == periodicidad[0]:
                return periodicidad[1]

    class Meta:
        verbose_name = 'Calendario'
        verbose_name_plural = 'Calendarios'

class DiaInvalido(models.Model):
    dia = models.DateField('Día', blank=False, null=False)

    def __str__(self):
        return self.dia

    class Meta:
        verbose_name = 'Día Inválido'
        verbose_name_plural = 'Días Inválidos'

class Presupuesto(models.Model):
    monto_inicial = models.DecimalField('Monto Inicial', max_digits=8, decimal_places=2, blank=False, null=False, validators=[MinValueValidator(0)])
    monto_disponible = models.DecimalField('Monto Disponible', max_digits=8, decimal_places=2, blank=False, null=False, validators=[MinValueValidator(0)])

    planilla_anual = models.ForeignKey(PlanillaAnual, null=False)
    unidad_organizacional = models.ForeignKey(UnidadOrganizacional, null=False)

    def __str__(self):
        return 'Presupuesto de %s para el año %s' % (self.unidad_organizacional, self.planilla_anual.año.__str__())

    class Meta:
        # Una Unidad Organizacional no puede tener más de un presupuesto para un mismo año
        unique_together = ('planilla_anual', 'unidad_organizacional',)
        verbose_name = 'Presupuesto'
        verbose_name_plural = 'Presupuestos'

class PuestoTrabajo(models.Model):
    nombre = models.CharField('Nombre', max_length=64, unique=True)
    activo = models.BooleanField('¿Puesto de Trabajo Activo?', default=True)
    salario_min = models.DecimalField('Salario Mínimo', max_digits=8, decimal_places=2, blank=False, null=False,validators=[MinValueValidator(0)])
    salario_max = models.DecimalField('Salario Máximo', max_digits=8, decimal_places=2, blank=False, null=False,validators=[MinValueValidator(0)])

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Puesto de Trabajo'
        verbose_name_plural = 'Puestos de Trabajo'

class EstructuraTerritorial(models.Model):
    nombre = models.CharField('Nombre', max_length=128)
    superdivision = models.ForeignKey('self', null=True, blank=True, related_name='subdivisiones')

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Estructura Territorial'
        verbose_name_plural = 'Estructuras Territoriales'


class Empleado(models.Model):
    primer_nombre = models.CharField('Primer nombre', max_length=32, null=False)
    segundo_nombre = models.CharField('Segundo nombre', max_length=32, blank=True, null=True)
    apellido_paterno = models.CharField('Apellido paterno', max_length=32, blank=True, null=True)
    apellido_materno = models.CharField('Apellido materno', max_length=32, blank=True, null=True)
    apellido_casada = models.CharField('Apellido casada', max_length=32, blank=True, null=True)

    dui = models.CharField('Número de DUI', max_length=10, help_text='Formato: XXXXXXXX-X', unique=True)
    nit = models.CharField('Número de NIT', max_length=17, help_text='Formato: XXXX-XXXXXX-XXX-X', unique=True)
    nup = models.CharField('Número de NUP', max_length=12, unique=True)
    isss = models.CharField('Número de ISSS', max_length=7, unique=True)
    pasaporte = models.CharField('Número de Pasaporte', max_length=9, unique=True, blank=True)

    fecha_nacimiento = models.DateField('Fecha de nacimiento', help_text='Formato: DD/MM/AAAA')
    fecha_ingreso = models.DateField('Fecha de ingreso', help_text='Formato: DD/MM/AAAA')

    telefono = models.CharField('Número de teléfono', max_length = 9, help_text='Formato: XXXX-XXXX',unique=True)
    email_personal = models.EmailField('Correo Electrónico Personal', max_length=256, unique=True)
    email_institucional = models.EmailField('Correo Electrónico Institucional', max_length=256, unique=True)
    direccion = models.CharField('Dirección', max_length = 64, help_text='Dirección de su residencia')
    domicilio = models.ForeignKey(EstructuraTerritorial, null=False, blank=False)

    activo = models.BooleanField('¿Empleado Activo?', default = True)
    salario_fijo = models.CharField('Tipo de Puesto', max_length = 1, default='1', choices=(('1', 'Salario Fijo'),('2', 'Salario por Comisión'),))


    genero = models.ForeignKey(Genero, null = False)
    estado_civil = models.ForeignKey(EstadoCivil, null = False)
    jefe = models.ForeignKey('self', null = True, related_name = 'subordinados', blank = True)
    puesto = models.ForeignKey(PuestoTrabajo, null = False, blank = False)
    profesion = models.ForeignKey(Profesion, null=True, blank=True)
    unidad_organizacional = models.ForeignKey(UnidadOrganizacional, null=False, default=1)
    salario = models.DecimalField('Salario', max_digits=8, decimal_places=2, blank=False, null=False,validators=[MinValueValidator(0)])

    def __str__(self):
        nombre = self.primer_nombre.title()
        if self.segundo_nombre:
            nombre += " " + self.segundo_nombre.title()
        if self.apellido_paterno:
            nombre += " " + self.apellido_paterno.title()
        if self.apellido_casada:
            nombre += "de" + self.apellido_casada.title()
        else:
            nombre += " " + self.apellido_materno.title()
        return nombre

    class Meta:
        ordering = ['primer_nombre']
        verbose_name = 'Empleado'
        verbose_name_plural = 'Empleados'

class Empresa(models.Model):
    nombre = models.CharField('Nombre', max_length = 64, null=False, unique = True)
    nit = models.CharField('Número de NIT', max_length=17, help_text='Formato: XXXX-XXXXXX-XXX-X', unique=True)
    nic = models.CharField('Número de NIC', max_length=7, unique=True)
    telefono = models.CharField('Número de teléfono', max_length=9, help_text='Formato: XXXX-XXXX', unique=True)
    webpage = models.CharField('Página Web', max_length = 64, unique = True, blank=True, null=True)
    email = models.CharField('Correo Electrónico', max_length = 64, unique = True)

    representante_legal = models.ForeignKey(Empleado, null = False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Empresa'
        verbose_name_plural = 'Empresas'

class DetallePlanilla(models.Model):
    nombre = models.CharField('Nombre', max_length = 130, blank=False, null=False)
    dui = models.CharField('Número de DUI', max_length=10, help_text='Formato: XXXXXXXX-X', unique=False, null=False)
    #dias_devengados = models.IntegerField('Días devengados', blank=False, null=True, validators=[MinValueValidator(0)])
    cargo = models.CharField('Cargo', max_length = 64, blank=False, null=False)
    departamento_id = models.IntegerField('Codigo Departamento',blank=False, null=False, default=1)
    departamento_nombre = models.CharField('Departamento', max_length = 130, blank=False, null=False, default='Ninguno')
    salario_base = models.DecimalField('Salario Base', max_digits=8, decimal_places=2, blank=False, null=False, validators=[MinValueValidator(0)])
    ventas = models.DecimalField('Ventas', max_digits=8, decimal_places=2, blank=False, null=True, default=0.00, validators=[MinValueValidator(0)])
    comision = models.DecimalField('Comisión', max_digits=8, decimal_places=2, blank=False, null=True, default=0.00, validators=[MinValueValidator(0)])
    # otros_ingresos_agravados = models.DecimalField('Otros Ingresos Agravados', max_digits=8, default=0.00, decimal_places=2, blank=False, validators=[MinValueValidator(0)])
    # otros_ingresos_exentos = models.DecimalField('Otros Ingresos Exentos', max_digits=8, default=0.00, decimal_places=2, blank=False, validators=[MinValueValidator(0)])
    total_ingresos = models.DecimalField('Total Ingresos', max_digits=8, decimal_places=2, null=True, default=0.00, blank=False, validators=[MinValueValidator(0)])
    renta = models.DecimalField('Renta', max_digits=8, decimal_places=2, blank=False, default=0.00, null=True, validators=[MinValueValidator(0)])
    # otros_descuentos = models.DecimalField('Otros Descuentos', max_digits=8, decimal_places=2, default=0.00, blank=False, validators=[MinValueValidator(0)])
    total_descuentos = models.DecimalField('Total Descuentos', max_digits=8, decimal_places=2, default=0.00, null=True, blank=False,  validators=[MinValueValidator(0)])
    total_retenciones = models.DecimalField('Total Retenciones', max_digits=8, decimal_places=2, default=0.00, null=True, blank=False,  validators=[MinValueValidator(0)])
    total_pagar = models.DecimalField('Total a Pagar', max_digits=8, decimal_places=2, default=0.00, null=True, blank=False,  validators=[MinValueValidator(0)])

    pagado = models.BooleanField('¿Está pagado?', default=False)

    calendario = models.ForeignKey(Calendario, null=False)
    empleado = models.ForeignKey(Empleado, null=False)

    def __str__(self):
        return 'Fecha: %s, Empleado: %s, Puesto: %s'  % (self.calendario.fecha_pago.__str__(), self.nombre, self.cargo)


    def actualizar_retenciones(self):
        cotizaciones = self.montocotizacion_set.all().values('cotizacion', 'monto')
        total_cotizaciones = 0
        for cot in cotizaciones:
            total_cotizaciones = total_cotizaciones + cot['monto']

        self.total_retenciones = total_cotizaciones + self.renta

        return 0

    def actualizar_total(self):
        self.total_pagar = self.salario_base + self.comision + self.total_ingresos - self.total_descuentos - self.total_retenciones
        return 0


    class Meta:
        verbose_name = 'Detalle de Planilla'
        verbose_name_plural = 'Detalles de Planilla'

class Cotizacion(models.Model):
    nombre = models.CharField('Nombre de la Cotización', max_length=16, blank=False, null=False, unique=True)
    porcentaje = models.DecimalField('Porcentaje', max_digits=5, decimal_places=2, blank=False, null=False, validators=[MinValueValidator(0), MaxValueValidator(100.00)])
    activo = models.BooleanField('¿Cotización Activa?', default=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Cotización'
        verbose_name_plural = 'Cotizaciones'


class MontoCotizacion(models.Model):
    monto = models.DecimalField('Monto de la Contización', max_digits=8, decimal_places=2, blank=False, null=False,validators=[MinValueValidator(0)])

    cotizacion = models.ForeignKey(Cotizacion, null=False)
    detalle_planilla = models.ForeignKey(DetallePlanilla, null=False)

    def __str__(self):
        return str(self.monto)

    class Meta:
        unique_together=('cotizacion', 'detalle_planilla',)
        verbose_name = 'Monto de Cotización'
        verbose_name_plural = 'Montos de Cotización'

class TipoIngreso(models.Model):
    nombre = models.CharField('Nombre del Tipo de Ingreso', max_length=32, blank=False, null=False, unique=True)
    gravado = models.CharField('Tipo de Ingreso', max_length = 1, default='1', choices=(('1', 'Gravado'),('2', 'Exento'),))
    activo = models.BooleanField('¿Tipo de Ingreso Activo?', default=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Tipo de Ingreso'
        verbose_name_plural = 'Tipos de Ingreso'

class MontoIngreso(models.Model):
    monto = models.DecimalField('Monto del Ingreso', max_digits=8, decimal_places=2, blank=False, null=False, validators=[MinValueValidator(0)])

    tipo_ingreso = models.ForeignKey(TipoIngreso, null=False)
    detalle_planilla = models.ForeignKey(DetallePlanilla, null=False)

    def __str__(self):
        return str(self.monto)

    class Meta:
        unique_together = ('tipo_ingreso', 'detalle_planilla',)
        verbose_name = 'Monto de Ingreso'
        verbose_name_plural = 'Montos de Ingreso'

class DescuentoPersonal(models.Model):
    nombre = models.CharField('Nombre del Descuento Personal', max_length=32, blank=False, null=False, unique=True)
    activo = models.BooleanField('¿Descuento Personal Activo?', default=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Descuento Personal'
        verbose_name_plural = 'Descuentos Personales'

class MontoDescuento(models.Model):
    monto = models.DecimalField('Monto del Descuento', max_digits=8, decimal_places=2, blank=False, null=False, validators=[MinValueValidator(0)])

    descuento_personal = models.ForeignKey(DescuentoPersonal, null=False)
    detalle_planilla = models.ForeignKey(DetallePlanilla, null=False)

    def __str__(self):
        return str(self.monto)

    class Meta:
        unique_together = ('descuento_personal', 'detalle_planilla',)
        verbose_name = 'Monto de Descuento '
        verbose_name_plural = 'Montos de Descuento'